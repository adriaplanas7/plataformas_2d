﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pimpom : MonoBehaviour
{


    public int platformSpeed;

    // Update is called once per frame
    void Update()
    {

        transform.Translate(0, platformSpeed * Time.deltaTime, 0);

    }


    void OnTriggerEnter2D(Collider2D other)
    {
        platformSpeed *= -1;
    }

}
