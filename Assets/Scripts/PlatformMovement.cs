﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMovement : MonoBehaviour
{


    public int platformSpeed;

    // Update is called once per frame
    void Update()
    {
        
        transform.Translate(platformSpeed*Time.deltaTime,0,0);

    }


    void OnTriggerEnter2D(Collider2D other)
    {
        platformSpeed*=-1;
    }

}
