﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class LookAtFollow : MonoBehaviour
{
    public float speed;
    private Transform target;
    Transform graphics;
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();

    }

    void Update()
    {
        {
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Finish")
        {
            Destroy(this.gameObject);
        }
        else if (other.tag == "Bullet")
        {
            StartCoroutine(Destroyfollow());
        }
    }

    IEnumerator Destroyfollow()
    {

        

        //Elimino el BoxCollider2D
        Destroy(GetComponent<BoxCollider2D>());

       

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        //Me destruyo a mi mismo
        Destroy(this.gameObject);
    }

}