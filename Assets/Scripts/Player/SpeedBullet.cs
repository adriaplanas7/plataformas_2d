﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBullet : Weapon
{
    public float speed;
    public float Damage= 10f;
    

    // Update is called once per frame
    void Update()
    {
        transform.Translate(speed*Time.deltaTime,0,0);
    }
    private void OnTriggerEnter2D(Collider2D other) {
       if(other.tag == "Finish"){
         Destroy(this.gameObject);
        }else if(other.tag =="Enemy"){
        Destroy(this.gameObject);
        }
        
    }

}
