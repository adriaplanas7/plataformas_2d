﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EasyWinSceneManager : MonoBehaviour
{

    public AudioSource click;

    public void PulsaRetry(){

        click.Play();
        Debug.LogError("He pulsado Play");
        SceneManager.LoadScene("EasyScene");
    }
    


    public void PulsaExit()
    {
        click.Play();
        SceneManager.LoadScene("MenuScene");
    }
}
