﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour
{
    private bool isPaused = false;
    [SerializeField] GameObject pauseCanvas;


    public void Resume(){
        pauseCanvas.SetActive(false);
        Time.timeScale = 1.0f;
        isPaused = false;
    }

    public void Exit(){
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("MenuScene");
    }


    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        if (!isPaused && Input.GetKeyDown(KeyCode.Escape))
        {
            ActivatePause();
        }else if(isPaused && Input.GetKeyDown(KeyCode.Escape)){
            Resume();
        }
    }

    void ActivatePause(){
        isPaused = true;

        pauseCanvas.SetActive(true);

        Time.timeScale = 0;
    }
}

  

