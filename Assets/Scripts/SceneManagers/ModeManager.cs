﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ModeManager : MonoBehaviour
{

    
    public void PulsaEasy(){

        

        SceneManager.LoadScene("EasyScene");
    }

    public void PulsaHardcore()
    {


        SceneManager.LoadScene("HardcoreScene");
    }

    public void PulsaBack()
    {

  

        SceneManager.LoadScene("MenuScene");
    }
}
