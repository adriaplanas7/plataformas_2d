﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverHardcoreManager : MonoBehaviour
{

    public AudioSource click;

    public void PulsaRetry(){

        click.Play();
        Debug.LogError("He pulsado Play");
        SceneManager.LoadScene("HardcoreScene");
    }
    


    public void PulsaQuit()
    {
        click.Play();
        SceneManager.LoadScene("MenuScene");
    }
}
